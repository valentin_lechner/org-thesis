* COMMENT Bibliography
This wont get exported, it is just there so that you can use the same bib file
in every part of this thesis
bibliography:../library.bib

* Hinweis zum Umfang der Arbeit
#+BEGIN_EXPORT latex
Der Textteil der vorliegenden Arbeit - beginnend mit der Einleitung bis
ausschließlich Anhang - umfasst \pageref{end} Seiten.
#+END_EXPORT
