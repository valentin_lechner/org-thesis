* COMMENT Bibliography
This wont get exported, it is just there so that you can use the same bib file
in every part of this thesis
bibliography:../library.bib
* Freigabe der Arbeit
#+BEGIN_EXPORT latex
Die vorliegende Arbeit wurde durch das Ausbildungsunternehmen \company,
 \companyLocation, inhaltlich geprüft und zur Vorlage
an der Dualen Hochschule Baden-Württemberg Lörrach, Studiengang \courseOfStudies, freigegeben.
\vspace*{4cm}
\begin{center}
\begin{tabular}{c c}
\noindent \rule{7cm}{0.5pt} & \noindent\rule{7cm}{0.5pt} \\
\noindent \releaseLocation, \releaseDate & Unterschrift Unternehmensvertreter \\
\end{tabular}
\end{center}
#+END_EXPORT
