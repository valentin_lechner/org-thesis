% Created 2020-06-26 Fri 11:26
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper,numbers=noenddot,titlepage,toc=bibliography, openany, oneside,toc=index,ngerman]{scrbook}
  \KOMAoptions{headings=small,fontsize=12,footheight=18pt}
\usepackage{ifpdf}
\usepackage{ifxetex}
\usepackage{ifluatex}
\newif\ifxetexorluatex
\ifxetex
\xetexorluatextrue
\else
\ifluatex
\xetexorluatextrue
\else
\xetexorluatexfalse
\fi
\fi

\ifxetexorluatex
\usepackage{fontspec}
\else
\usepackage[utf8]{inputenc}
\fi

% Makes it possible to switch between different languages in the text
% while keeping hyphenation rules correct. Should you add another one
% in the list, please ensure that `english` is the last one. The last
% language is used to control standard hyphenation.
\usepackage[ngerman, germanb]{babel}

\usepackage{makeidx}  % For creating indices
\usepackage{xspace}   % For automatically "eating" spaces

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Multi-line comments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\comment}[1]{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonts & colours
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[usenames,dvipsnames]{xcolor}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Graphics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{graphicx}
\graphicspath{%
{Figures/}
{./}
}

% Suppress warnings about page groups in PDFs. This is not justified
% in most of the cases. I am pretty sure I am including my images in
% the right manner.
\begingroup\expandafter\expandafter\expandafter\endgroup
\expandafter\ifx\csname pdfsuppresswarningpagegroup\endcsname\relax
\else
\pdfsuppresswarningpagegroup=1\relax
\fi

\usepackage{subcaption}

% Make sub-references using \subref being typeset with parentheses.
% Otherwise, only the counter will be printed.
\captionsetup{subrefformat=parens}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Glossaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[%
acronym,
automake,
nogroupskip,
nopostdot,
nonumberlist,
toc,
]{glossaries}

% New style that prevents line-breaks between the full description and
% the acronym. Furthermore, it ensures that the acronym is always
% printed in an upright font.
\newacronymstyle{long-short-mimosis}
{%
\GlsUseAcrEntryDispStyle{long-short}%
}%
{%
\GlsUseAcrStyleDefs{long-short}%
\renewcommand*{\genacrfullformat}[2]{%
\glsentrylong{##1}##2~\textup{(\firstacronymfont{\glsentryshort{##1}})}%
}%
\renewcommand*{\Genacrfullformat}[2]{%
\Glsentrylong{##1}##2~\textup{(\firstacronymfont{\glsentryshort{##1}})}%
}%
\renewcommand*{\genplacrfullformat}[2]{%
\glsentrylongpl{##1}##2~\textup{(\firstacronymfont{\glsentryshortpl{##1}})}%
}%
\renewcommand*{\Genplacrfullformat}[2]{%
\Glsentrylongpl{##1}##2~\textup{(\firstacronymfont{\Glsentryshortpl{##1}})}%
}%
}

% A new glossary style that based on the long style of the glossaries
% package. It ensures that descriptions and entries are aligned.
\newglossarystyle{long-mimosis}{%
\setglossarystyle{long}

\renewcommand{\glossentry}[2]{%
\glsentryitem{##1}\glstarget{##1}{\glossentryname{##1}} &
\glossentrydesc{##1}\glspostdescription\space ##2\tabularnewline
}%
}

% Constrain the description width to enforce breaks.
\setlength{\glsdescwidth}{10cm}

\setacronymstyle{long-short-mimosis}
\setglossarystyle{long-mimosis}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Paragraph lists & compact enumerations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[%
olditem,  % Do not modify itemize environments by default
oldenum   % Do not modify enumerate environments by default
]{paralist}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{setspace}
\onehalfspacing

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{booktabs}
\usepackage{multirow}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Proper typesetting of units
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[binary-units=true]{siunitx}

\sisetup{%
detect-all           = true,
detect-family        = true,
detect-mode          = true,
detect-shape         = true,
detect-weight        = true,
detect-inline-weight = math,
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mathematics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{dsfont}

% Fix the spacing of \left and \right. Use these with the proper bracket
% in order to ensure that they scale automatically.
\let\originalleft\left
\let\originalright\right
\renewcommand{\left}{\mathopen{}\mathclose\bgroup\originalleft}
\renewcommand{\right}{\aftergroup\egroup\originalright}

\DeclareMathOperator*{\argmin}          {arg\,min}
\DeclareMathOperator {\dist}            {dist}
\DeclareMathOperator {\im}              {im}

\newcommand{\domain}{\ensuremath{\mathds{D}}}
\newcommand{\real}  {\ensuremath{\mathds{R}}}

% Proper differential operators
\newcommand{\diff}[1]{\ensuremath{\operatorname{d}\!{#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ordinals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand  {\st}{\textsuperscript{\textup{st}}\xspace}
\newcommand  {\rd}{\textsuperscript{\textup{rd}}\xspace}
\newcommand  {\nd}{\textsuperscript{\textup{nd}}\xspace}
\renewcommand{\th}{\textsuperscript{\textup{th}}\xspace}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Penalties
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clubpenalty         = 10000
\widowpenalty        = 10000
\displaywidowpenalty = 10000

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Headers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{scrlayer-scrpage}
\pagestyle{scrheadings}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Typefaces for parts, chapters, and sections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\partformat}{\huge\partname~\thepart\autodot}
\renewcommand{\raggedpart}{\flushleft}

\setkomafont{part}{\normalfont\huge\scshape}

\setkomafont{sectioning}{\normalfont\scshape}
\setkomafont{descriptionlabel}{\normalfont\bfseries}

\setkomafont{caption}{\small}
\setkomafont{captionlabel}{\usekomafont{caption}}

% Large number for chapter
\renewcommand*{\chapterformat}{%
\fontsize{50}{55}\selectfont\thechapter\autodot\enskip
}
\newcounter{pageNumber}
\usepackage{titling}
\usepackage[head=\baselineskip,left=3.0cm,right=3.0cm,top=2.5cm,bottom=3.0cm]{geometry}
\usepackage{graphicx}
\graphicspath{ {../images/} }
\renewcommand{\floatpagefraction}{.8}%
\usepackage{etoolbox}
\DeclareSIUnit\px{px}
\usepackage[linktocpage, pdfstartview=FitH,colorlinks, linkcolor=RoyalBlue,anchorcolor=RoyalBlue, citecolor=RoyalBlue,filecolor=RoyalBlue,menucolor=RoyalBlue,urlcolor=RoyalBlue,unicode,]{hyperref}
\usepackage{bookmark}
\bookmarksetup{depth=2}
\usepackage{bbding}
\usepackage{hyphenat}
\hyphenation{deoxy-hemo-glo-bin}
\hyphenation{Sie-mens}
\hyphenation{multi-band multi-echo}
\usepackage[level]{datetime}
\usepackage[autocite=plain,doi=true,url=true,giveninits=true,hyperref=true,maxbibnames=99,maxcitenames=99,sortcites=true,style=numeric,backend=biber]{biblatex}
\input{bibliography-mimosis}
\AtEveryBibitem{%
\ifentrytype{online}{}{% Remove url except for @online
\clearfield{url}
}
}
\setlength\bibitemsep{1.1\itemsep}
\renewcommand*{\bibfont}{\footnotesize}
\usepackage[sf,scaled=0.9]{quattrocento}
\usepackage[lf]{ebgaramond}
\usepackage[oldstyle, scale=0.7]{sourcecodepro}
\setkomafont{sectioning}{\normalfont\bfseries}
\renewcommand{\th}{\textsuperscript{\textup{th}}\xspace}
\makeatletter
\renewcommand*{\chapterformat}{  \mbox{\chapappifchapterprefix{\nobreakspace}{\color{RoyalBlue}\fontsize{40}{45}\selectfont\thechapter}\autodot\enskip}}
\renewcommand\@seccntformat[1]{\color{RoyalBlue} {\csname the#1\endcsname}\hspace{0.3em}}
\makeatother
\newenvironment*{abstract}{}{}
\setparsizes{0em}{0.1\baselineskip plus .1\baselineskip}{1em plus 1fil}
\setcounter{tocdepth}{1}
\usepackage{floatrow}
\floatsetup[table]{font={footnotesize,sf},capposition=top}
\numberwithin{equation}{chapter}
\usepackage{listings}
\usepackage{minted}
\setminted{autogobble=true,fontsize=\small,baselinestretch=0.8}
\setminted[python]{python3=true,tabsize=4}
\usemintedstyle{trac}
\lstset{abovecaptionskip=0}
\numberwithin{listing}{chapter}
\renewcommand{\listingscaption}{Code Snippet}
\AtEndEnvironment{listing}{\vspace{-16pt}}
\renewcommand{\listoflistingscaption}{Quellcodeverzeichnis}
\usepackage{csquotes}
\def\signed #1{{\leavevmode\unskip\nobreak\hfil\penalty50\hskip1em
\hbox{}\nobreak\hfill #1%
\parfillskip=0pt \finalhyphendemerits=0 \endgraf}}
\newsavebox\mybox
\newenvironment{aquote}[1]
{\savebox\mybox{#1}\begin{quote}\openautoquote\hspace*{-.7ex}}
{\unskip\closeautoquote\vspace*{1mm}\signed{\usebox\mybox}\end{quote}}
\usepackage{scrhack}
\usepackage{glossaries}
\usepackage[toc,page]{appendix}
\renewcommand{\appendixpagename}{\appendixname}
\renewcommand{\appendixtocname}{\appendixname}
\renewcommand\appendix{
\par
\setcounter{chapter}{0}
\setcounter{section}{0}
\setcounter{subsection}{0}
\renewcommand\thechapter{}
\renewcommand\thefigure{\Alph{chapter}.\arabic{figure}}
}
\newif\ifblockingnotice
\newcommand{\thesisTitle}{{\thetitle}}
\newcommand{\thesisSubtitle}{{is nice}}
\newcommand{\thesisType}{{Bachelorarbeit}}
\newcommand{\degree}{{Bachelor of Science}}
\newcommand{\submissionDate}{{\mbox{\formatdate{21}{9}{2020}}}}
\newcommand{\courseOfStudies}{{Angewandte Informatik}}
\newcommand{\university}{{Duale Hochschule Baden-Württemberg Lörrach}}
\newcommand{\course}{{TIF17A}}
\newcommand{\authorName}{{Author Name}}
\newcommand{\company}{{E-Corp}}
\newcommand{\companyLocation}{{Earth}}
\newcommand{\corporateAdvisor}{{Prof.\ dr. Doc Doctor}}
\newcommand{\universityAdvisor}{{Prof.\ dr. Doctor Doc}}
\newcommand{\declarationLocation}{{\companyLocation}}
\newcommand{\declarationDate}{{\mbox{\formatdate{24}{6}{2020}}}}
\newcommand{\releaseLocation}{{\companyLocation}}
\newcommand{\releaseDate}{{\submissionDate}}
\newcommand{\blockingNoticeDate}{{\declarationDate}}
\newcommand{\companyAddress}{{Lagistraße \\ 79100 Freiburg \\ Deutschland}}
\newcommand{\companyPhone}{{0100 8 33 11}}
\newcommand{\companyMail}{{info@example.com}}
\blockingnoticetrue
\newcommand{\blockingNotice}{{\textcolor{red}{mit Sperrvermerk}}}
\bibliography{../library}
\newglossaryentry{LaTeX}{name={\LaTeX},description={A document preparation system},sort={LaTeX}}
\newglossaryentry{Real Numbers}{name={$\real$},description={The set of Real numbers},sort={Real Numbers}}
\makeglossaries

\newcommand{\mboxparagraph}[1]{\paragraph{#1}\mbox{}\\}
\newcommand{\mboxsubparagraph}[1]{\subparagraph{#1}\mbox{}\\}
\author{Valentin Lechner}
\date{\today}
\title{Writing a thesis in Org Mode}
\hypersetup{
 pdfauthor={Valentin Lechner},
 pdftitle={Writing a thesis in Org Mode},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.4)}, 
 pdflang={Germanb}}
\begin{document}



\frontmatter

  \begin{titlepage}
    %\BgThispage
    \pagestyle{empty}
    \vspace*{-3cm}
    \includegraphics[width=4.5cm]{company_logo}
    \hfill
    \includegraphics[width=4.5cm]{university_logo}
    \begin{center}
      \null\vfill
      {\LARGE{\bfseries \thesisTitle}\par}
      \vspace{\stretch{0.5}}
      {\large \thesisSubtitle \par}
      \ifblockingnotice
      {\LARGE \blockingNotice \par}
      \else
      \fi
      \vspace{\stretch{2}}
      {\thesisType \par}
      \vspace{\stretch{1}}
      für die Prüfung\\
      zum {\degree \par}
      \vspace{\stretch{0.5}}
      {des Studiengangs \courseOfStudies \par}
      {\university \par}
      \vspace{\stretch{1}}
      von\\
      \vspace{\stretch{1}}
      {\large\authorName}\\
      \vspace{\stretch{1}}
      {\submissionDate \par}
      \vfill
    \end{center}

    \clearpage
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Sixth page: Corona Information (English)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \noindent
    \begin{tabular}{@{}l@{\hspace{22pt}}l}
      \textbf{Kurs}:                    & \course \\

\\

      \textbf{Ausbildungsfirma}  & \company, \companyLocation \\

\\

      \textbf{Betreuer der Ausbildungsfirma} & \corporateAdvisor \\

\\

      \textbf{Wissenschaftlicher Betreuer} & \universityAdvisor \\

    \end{tabular}
    \cleardoublepage
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Second page: Copyright and ISSN Number
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \null
    \noindent
    The work presented in the current demonstration could not have been possible
    without wonderful blog posts on Org mode usage found on the internet,
    and without the GNU Emacs documentation.


    \vfill
    \noindent\textcopyleft\ \number\year, \authorName\\
    \thesisTitle\\
    Thesis, GNUniversity, The Earth\\
    Illustrated; with bibliographic information

    \bigskip
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % End Titlepage
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \end{titlepage}
\chapter{Ehrenwörtliche Erklärung}
\label{sec:org5cbf631}
Ich versichere hiermit, dass ich meine {\thesisType} mit dem Thema
\begin{center}
\thesisTitle
\end{center}
selbstständig verfasst und keine anderen als die angegebenen Quellen und
Hilfsmittel benutzt habe. Ich versichere zudem, dass die eingereichte
elektronische Fassung mit der gedruckten Fassung übereinstimmt.


\vspace*{1.8cm}
\noindent\rule{8cm}{0.5pt} \\
\authorName, \declarationLocation, \declarationDate \\
\vspace*{3cm} \\
\chapter{Hinweis zum Umfang der Arbeit}
\label{sec:org0b07fda}
Der Textteil der vorliegenden Arbeit - beginnend mit der Einleitung bis
ausschließlich Anhang - umfasst \pageref{end} Seiten.
\chapter{Freigabe der Arbeit}
\label{sec:orgef6f2fb}
Die vorliegende Arbeit wurde durch das Ausbildungsunternehmen \company,
 \companyLocation, inhaltlich geprüft und zur Vorlage
an der Dualen Hochschule Baden-Württemberg Lörrach, Studiengang \courseOfStudies, freigegeben.
\vspace*{4cm}
\begin{center}
\begin{tabular}{c c}
\noindent \rule{7cm}{0.5pt} & \noindent\rule{7cm}{0.5pt} \\
\noindent \releaseLocation, \releaseDate & Unterschrift Unternehmensvertreter \\
\end{tabular}
\end{center}
\ifblockingnotice
\chapter{Sperrvermerk}
\label{sec:org00bf9eb}
Der Inhalt dieser Arbeit darf weder als Ganzes noch in Auszügen Personen
außerhalb des Prüfungsprozesses und des Evaluationsverfahrens zugänglich gemacht
werden, sofern keine anders lautende Genehmigung der Ausbildungsstätte vorliegt.
\vspace*{2cm}
\begin{center}
\begin{tabular}{c c}
\noindent\rule{7cm}{0.5pt} & \noindent\rule{7cm}{0.5pt} \\
\noindent\blockingNoticeDate, \authorName & \blockingNoticeDate, \company \\
\end{tabular}
\end{center}
\vspace*{2cm}
Kontakt: \\
\company \\
\companyLocation \\
Tel.: \companyPhone \\
E-Mail: \companyMail \\
\fi
\chapter{Kurzfassung}
\label{sec:orgfb31a5b}
Es gibt im Moment in diese Mannschaft, oh, einige Spieler vergessen ihnen Profi
was sie sind. Ich lese nicht sehr viele Zeitungen, aber ich habe gehört viele
Situationen. Erstens: wir haben nicht offensiv gespielt. Es gibt keine deutsche
Mannschaft spielt offensiv und die Name offensiv wie Bayern. Letzte Spiel hatten
wir in Platz drei Spitzen: Elber, Jancka und dann Zickler. Wir müssen nicht
vergessen Zickler. Zickler ist eine Spitzen mehr, Mehmet eh mehr Basler. Ist
klar diese Wörter, ist möglich verstehen, was ich hab gesagt? Danke. Offensiv,
offensiv ist wie machen wir in Platz. Zweitens: ich habe erklärt mit diese zwei
Spieler: nach Dortmund brauchen vielleicht Halbzeit Pause. Ich habe auch andere
Mannschaften gesehen in Europa nach diese Mittwoch. Ich habe gesehen auch zwei
Tage die Training. Ein Trainer ist nicht ein Idiot! Ein Trainer sei sehen was
passieren in Platz. In diese Spiel es waren zwei, drei diese Spieler waren
schwach wie eine Flasche leer! Haben Sie gesehen Mittwoch, welche Mannschaft hat
gespielt Mittwoch? Hat gespielt Mehmet oder gespielt Basler oder hat gespielt
Trapattoni? Diese Spieler beklagen mehr als sie spielen! Wissen Sie, warum die
Italienmannschaften kaufen nicht diese Spieler? Weil wir haben gesehen viele
Male solche Spiel! Haben gesagt sind nicht Spieler für die italienisch Meisters!
Strunz! Strunz ist zwei Jahre hier, hat gespielt 10 Spiele, ist immer verletzt!
Was erlauben Strunz? Letzte Jahre Meister Geworden mit Hamann, eh, Nerlinger.
Diese Spieler waren Spieler! Waren Meister geworden! Ist immer verletzt! Hat
gespielt 25 Spiele in diese Mannschaft in diese Verein. Muß respektieren die
andere Kollegen! haben viel nette kollegen! Stellen Sie die Kollegen die Frage!
Haben keine Mut an Worten, aber ich weiß, was denken über diese Spieler. Mussen
zeigen jetzt, ich will, Samstag, diese Spieler müssen zeigen mich, seine Fans,
müssen alleine die Spiel gewinnen. Ich bin müde jetzt Vater diese Spieler, eh
der Verteidiger diese Spieler. Ich habe immer die Schuld über diese Spieler.
Einer ist Mario, einer andere ist Mehmet! Strunz ich spreche nicht, hat gespielt
nur 25 Prozent der Spiel. Ich habe fertig! \ldots{}wenn es gab Fragen, ich kann Worte
wiederholen\ldots{}
\tableofcontents
\clearpage
\listoffigures
\clearpage
\listoftables
\clearpage
\clearpage
\listoflistings
\clearpage
\setcounter{pageNumber}{\value{page}}
\mainmatter

\chapter{Introduction\label{chap:01-intro}}
\label{sec:orgb9ab560}
We can have equations and reference them using \texttt{eqref}, as seen in \eqref{larmor}.

\begin{equation}
\omega_{0} = \gamma B_{0} \label{larmor}
\end{equation}


And we can have some code as well, as seen in code snippet \ref{code}.

\begin{listing}[htbp]
\begin{minted}[]{elisp}
(setq phd-thesis-status 'finishing)
\end{minted}
\caption{\label{code}Example code.}
\end{listing}

Acronyms can also be used. In this case, I use the glossaryentry \gls{LaTeX}

Nullam eu ante vel est convallis dignissim.  Fusce suscipit, wisi nec facilisis
facilisis, est dui fermentum leo, quis tempor ligula erat quis odio.  Nunc porta
vulputate tellus.  Nunc rutrum turpis sed pede.  Sed bibendum.  Aliquam posuere.
Nunc aliquet, augue nec adipiscing interdum, lacus tellus malesuada massa, quis
varius mi purus non odio.  Pellentesque condimentum, magna ut suscipit
hendrerit, ipsum augue ornare nulla, non luctus diam neque sit amet urna.
Curabitur vulputate vestibulum lorem.  Fusce sagittis, libero non molestie
mollis, magna orci ultrices dolor, at vulputate neque nulla lacinia eros.  Sed
id ligula quis est convallis tempor.  Curabitur lacinia pulvinar nibh.  Nam a
sapien.

Nullam eu ante vel est convallis dignissim.  Fusce suscipit, wisi nec facilisis
facilisis, est dui fermentum leo, quis tempor ligula erat quis odio.  Nunc porta
vulputate tellus.  Nunc rutrum turpis sed pede.  Sed bibendum.  Aliquam posuere.
Nunc aliquet, augue nec adipiscing interdum, lacus tellus malesuada massa, quis
varius mi purus non odio.  Pellentesque condimentum, magna ut suscipit
hendrerit, ipsum augue ornare nulla, non luctus diam neque sit amet urna.
Curabitur vulputate vestibulum lorem.  Fusce sagittis, libero non molestie
mollis, magna orci ultrices dolor, at vulputate neque nulla lacinia eros.  Sed
id ligula quis est convallis tempor.  Curabitur lacinia pulvinar nibh.  Nam a
sapien.

Nullam eu ante vel est convallis dignissim.  Fusce suscipit, wisi nec facilisis
facilisis, est dui fermentum leo, quis tempor ligula erat quis odio.  Nunc porta
vulputate tellus.  Nunc rutrum turpis sed pede.  Sed bibendum.  Aliquam posuere.
Nunc aliquet, augue nec adipiscing interdum, lacus tellus malesuada massa, quis
varius mi purus non odio.  Pellentesque condimentum, magna ut suscipit
hendrerit, ipsum augue ornare nulla, non luctus diam neque sit amet urna.
Curabitur vulputate vestibulum lorem.  Fusce sagittis, libero non molestie
mollis, magna orci ultrices dolor, at vulputate neque nulla lacinia eros.  Sed
id ligula quis est convallis tempor.  Curabitur lacinia pulvinar nibh.  Nam a
sapien.
Aliquam erat volutpat. \footcite{big} Nunc eleifend leo vitae magna, as discussed
in \footcite{small}.  In id erat non orci commodo lobortis.  Proin neque massa, cursus
ut, gravida ut, lobortis eget, lacus.  Sed diam.  Praesent fermentum tempor
tellus.  Nullam tempus.  Mauris ac felis vel velit tristique imperdiet.  Donec
at pede.  Etiam vel neque nec dui dignissim bibendum.  Vivamus id enim.
Phasellus neque orci, porta a, aliquet quis, semper a, massa.  Phasellus purus.
Pellentesque tristique imperdiet tortor.  Nam euismod tellus id erat.
Pellentesque dapibus suscipit ligula.  Donec posuere augue in quam.  Etiam vel
tortor sodales tellus ultricies commodo.  Suspendisse potenti.  Aenean in sem ac
leo mollis blandit.  Donec neque quam, dignissim in, mollis nec, sagittis eu,
wisi.  Phasellus lacus.  Etiam laoreet quam sed arcu.  Phasellus at dui in
ligula mollis ultricies.  Integer placerat tristique nisl.  Praesent augue.
Fusce commodo.  Vestibulum convallis, lorem a tempus semper, dui dui euismod
elit, vitae placerat urna tortor vitae lacus.  Nullam libero mauris, consequat
quis, varius et, dictum id, arcu.  Mauris mollis tincidunt felis.  Aliquam
feugiat tellus ut neque.  Nulla facilisis, risus a rhoncus fermentum, tellus
tellus lacinia purus, et dictum nunc justo sit amet elit.
Nullam eu ante vel est convallis dignissim.  Fusce suscipit, wisi nec facilisis
facilisis, est dui fermentum leo, quis tempor ligula erat quis odio.  Nunc porta
vulputate tellus.  Nunc rutrum turpis sed pede.  Sed bibendum.  Aliquam posuere.
Nunc aliquet, augue nec adipiscing interdum, lacus tellus malesuada massa, quis
varius mi purus non odio.  Pellentesque condimentum, magna ut suscipit
hendrerit, ipsum augue ornare nulla, non luctus diam neque sit amet urna.
Curabitur vulputate vestibulum lorem.  Fusce sagittis, libero non molestie
mollis, magna orci ultrices dolor, at vulputate neque nulla lacinia eros.  Sed
id ligula quis est convallis tempor.  Curabitur lacinia pulvinar nibh.  Nam a
sapien.

\section{Data Acquisition\label{sec:01-acq}}
\label{sec:org81db752}
We can have links everywhere.  This \href{https://www.gnu.org/software/emacs/}{link}, for example, points to the Emacs
website.
Aliquam erat volutpat.  Nunc eleifend leo vitae magna.  In id erat non orci
commodo lobortis.  Proin neque massa, cursus ut, gravida ut, lobortis eget,
lacus.  Sed diam.  Praesent fermentum tempor tellus.  Nullam tempus.  Mauris ac
felis vel velit tristique imperdiet.  Donec at pede.  Etiam vel neque nec dui
dignissim bibendum.  Vivamus id enim.  Phasellus neque orci, porta a, aliquet
quis, semper a, massa.  Phasellus purus.  Pellentesque tristique imperdiet
tortor.  Nam euismod tellus id erat.
It can contain tables as well, with captions, of course, as seen in figure
\ref{example-table}.
\begin{table}[htbp]
\caption{\label{example-table}Table with a custom caption.}
\centering
\begin{tabular}{rrr}
\toprule
foo & bar & baz\\
1 & 2 & 3\\
\bottomrule
\end{tabular}
\end{table}
Lorem ipsum dolor sit amet, consectetuer adipiscing elit.  Donec hendrerit
tempor tellus.  Donec pretium posuere tellus.  Proin quam nisl, tincidunt et,
mattis eget, convallis nec, purus.  Cum sociis natoque penatibus et magnis dis
parturient montes, nascetur ridiculus mus.  Nulla posuere.  Donec vitae dolor.
Nullam tristique diam non turpis.  Cras placerat accumsan nulla.  Nullam rutrum.
Nam vestibulum accumsan nisl.
Nullam eu ante vel est convallis dignissim.  Fusce suscipit, wisi nec facilisis
facilisis, est dui fermentum leo, quis tempor ligula erat quis odio.  Nunc porta
vulputate tellus.  Nunc rutrum turpis sed pede.  Sed bibendum.  Aliquam posuere.
Nunc aliquet, augue nec adipiscing interdum, lacus tellus malesuada massa, quis
varius mi purus non odio.  Pellentesque condimentum, magna ut suscipit
hendrerit, ipsum augue ornare nulla, non luctus diam neque sit amet urna.
Curabitur vulputate vestibulum lorem.  Fusce sagittis, libero non molestie
mollis, magna orci ultrices dolor, at vulputate neque nulla lacinia eros.  Sed
id ligula quis est convallis tempor.  Curabitur lacinia pulvinar nibh.  Nam a
sapien.

\section{Conclusion\label{sec:01-concl}}
\label{sec:org14a9ffe}
Awesome.
\chapter{Summary\label{end}}
\label{sec:orgeda09b6}
Nullam eu ante vel est convallis dignissim.  Fusce suscipit, wisi nec facilisis
facilisis, est dui fermentum leo, quis tempor ligula erat quis odio.  Nunc porta
vulputate tellus.  Nunc rutrum turpis sed pede.  Sed bibendum.  Aliquam posuere.
Nunc aliquet, augue nec adipiscing interdum, lacus tellus malesuada massa, quis
varius mi purus non odio.  Pellentesque condimentum, magna ut suscipit
hendrerit, ipsum augue ornare nulla, non luctus diam neque sit amet urna.
Curabitur vulputate vestibulum lorem.  Fusce sagittis, libero non molestie
mollis, magna orci ultrices dolor, at vulputate neque nulla lacinia eros.  Sed
id ligula quis est convallis tempor.  Curabitur lacinia pulvinar nibh.  Nam a
sapien.
\clearpage
\pagenumbering{roman}
\setcounter{page}{\value{pageNumber}}
\begin{appendices}
\chapter{Bilder}
\label{sec:orgca6e92a}
\chapter{Tabellen}
\label{sec:orgdd21cca}

\end{appendices}
\bookmarksetup{startatroot}
\backmatter
\chapter{Glossary}
\label{sec:orgbd6e528}
\begingroup
  \let\clearpage\relax
  \glsaddall
  \printglossary[type=\acronymtype]
  \newpage
  \printglossary
\endgroup
  \begingroup
    \let\clearpage\relax
    \glsaddall
    \printglossary[type=\acronymtype]
    \newpage
    \printglossary
  \endgroup
\printbibliography
\end{document}